from gpiozero import TrafficLights
from time import sleep

red = 17
amber = 27
green = 22

lights = TrafficLights(red, amber, green)
lights.green.on()

while True:
    sleep(10)
    lights.green.off()
    lights.amber.on()
    sleep(1)
    lights.amber.off()
    lights.red.on()
    sleep(10)
    lights.amber.on()
    sleep(1)
    lights.green.on()
    lights.amber.off()
    lights.red.off()
